var script = document.createElement("script");
script.src = chrome.runtime.getURL("/js/app_official.js");
(document.head || document.documentElement).appendChild(script);

var bootstrap_script = document.createElement("script");
bootstrap_script.src = chrome.runtime.getURL(
  "/assets/bootstrap/js/bootstrap.min.js"
);
(document.head || document.documentElement).appendChild(bootstrap_script);

var css = document.createElement("LINK");
css.setAttribute("rel", "stylesheet");
css.setAttribute("type", "text/css");
css.setAttribute("href", chrome.runtime.getURL("/css/stylesheet.css"));
document.head.appendChild(css);

var bootstrap_css = document.createElement("LINK");
bootstrap_css.setAttribute("rel", "stylesheet");
bootstrap_css.setAttribute("type", "text/css");
bootstrap_css.setAttribute(
  "href",
  chrome.runtime.getURL("/assets/bootstrap/css/bootstrap.min.css")
);
document.head.appendChild(bootstrap_css);
