let details = document.querySelector("#pkgdetails");
let div = document.createElement("div");

let package = {
  version: document.querySelector('meta[itemprop="version"]').content,
  name: document.querySelector('meta[itemprop="name"]').content,
};

div.className = "aur_installer box";

div.innerHTML = `
<h2 class="aur-install-title noborder">Arch Package Installer <small class="text-muted size-small">(Click on the button's to copy the command to clipboard!)</small></h2>
<div class="btnlist">
  <div id="btnInstall" class="btn btn-primary noselect mr-1">
    <i class="bi bi-clipboard-plus"></i> Install
  </div>
  <div id="btnRemove" class="btn btn-danger noselect">
  <i class="bi bi-clipboard-minus"></i> Remove</div>
</div>`;

details.parentNode.insertBefore(div, details);

function clipboard(command) {
  navigator.clipboard.writeText(command).then(
    function () {},
    function (err) {
      console.error("Could not copy text: ", err);
    }
  );
}
document.getElementById("btnInstall").addEventListener("click", install);
document.getElementById("btnRemove").addEventListener("click", remove);
function install() {
  let command = "sudo pacman -Sy " + package.name;
  clipboard(command);
}

function remove() {
  let command = "sudo pacman -R " + package.name;
  clipboard(command);
}
