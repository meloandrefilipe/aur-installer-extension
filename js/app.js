let search = document.querySelector("#pkglist-search");
let div = document.createElement("div");

let package = {
  url: "",
  name: "",
};

div.className = "aur_installer box";

div.innerHTML = `
<h2 class="aur-install-title noborder">Arch Package Installer <small class="text-muted size-small">(Click on the button's to copy the command to clipboard!)</small></h2>
<div class="btnlist">
  <div class="btn btn-primary noselect mr-1" onclick="install()">
    <i class="bi bi-clipboard-plus"></i> Install
  </div>
  <div class="btn btn-danger noselect" onclick="remove()">
  <i class="bi bi-clipboard-minus"></i> Remove</div>
</div>


    `;

search.parentNode.insertBefore(div, search.nextSibling);

let copys = document.getElementsByClassName("copy");
let wraps = document.getElementsByClassName("wrap");
package.url = copys[0].getAttribute("href");
package.name = wraps[0].childNodes[0].innerHTML;

function clipboard(command) {
  navigator.clipboard.writeText(command).then(
    function () {},
    function (err) {
      console.error("Could not copy text: ", err);
    }
  );
}

function install() {
  let command =
    'cd /tmp && rm -Rf "$(basename ' +
    package.url +
    ' .git)" && git clone ' +
    package.url +
    ' && cd "$(basename ' +
    package.url +
    ' .git)" && makepkg -si ';
  clipboard(command);
}

function remove() {
  let command = "sudo pacman -R " + package.name;
  clipboard(command);
}
